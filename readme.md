# Octopus Google Cloud Storage for Laravel

A Google Cloud Storage filesystem for Laravel.

## Installation

Add in composer.json in repository list

```bash
{
    "type": "vcs",
    "url": "https://gitlab.com/octopus-srl/octopus-google-cloud-storage.git"
}
```

Then you can run

```bash
composer require octopus/google-cloud-storage
```

Add in config/app.php

```bash
\Octopus\GoogleCloudStorage\OctopusGoogleCloudStorageServiceProvider::class
```

Add a new disk to your `filesystems.php` config

```php
'gcs' => [
    "driver"              => "gcs",
    "bucketName"          => env('GOOGLE_CLOUD_STORAGE_BUCKET', 'octopus-uploader'),
    'folder'              => env('GOOGLE_CLOUD_STORAGE_FOLDER', 'octopus-uploader'),
    'keyFile'             => 'google-cloud.json', // path to file
    "encryptionKey"       => "",
    "encryptionKeySHA256" => "",
    "expireMinutes"       => "15",
    'storageApiUri'       => env('GOOGLE_CLOUD_STORAGE_API_URI', null), // see: Public URLs below
    'visibility'          => 'public', // optional: public|private
]
```