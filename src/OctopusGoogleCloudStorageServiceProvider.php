<?php
namespace Octopus\GoogleCloudStorage;

use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class OctopusGoogleCloudStorageServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Storage::extend('gcs', function ($app, $config) {
            $googleCloudStorageClient = new OctopusGoogleCloudStorageClient();

            return new Filesystem(new OctopusGoogleCloudStorageAdapter(
                $googleCloudStorageClient->client,
                $googleCloudStorageClient->bucket,
                $googleCloudStorageClient->folder,
                $googleCloudStorageClient->storageApiUri
            ));

        });
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}