<?php

namespace Octopus\GoogleCloudStorage;

use Carbon\Carbon;
use DateTime;
use Google\Cloud\Storage\Bucket;
use Google\Cloud\Storage\StorageClient as GoogleStorageClient;
use Illuminate\Support\Arr;

class OctopusGoogleCloudStorageClient
{
    /** @var GoogleStorageClient */
    public $client;

    /** @var Bucket $bucket */
    public $bucket;

    /** @var string */
    public $encryptionKey;

    /** @var string */
    public $folder;

    /** @var string */
    public $encryptionKeySHA256;

    /** @var string */
    public $storageApiUri;

    /** @var DateTime */
    public $expires;

    /**
     * GoogleCloudUploadClient constructor.
     */
    public function __construct()
    {

        $configs = Arr::get(config('filesystems'), 'disks.gcs', [
            "bucketName"          => "",
            'folder'              => "",
            'keyFile'             => "",
            "encryptionKey"       => "",
            "encryptionKeySHA256" => "",
            "expireMinutes"       => "",
            "storageApiUri"       => "",
        ]);

        $keyFilePath = Arr::get($configs, 'keyFile', '');

        $keyFile = file_exists($keyFilePath)
            ? file_get_contents($keyFilePath)
            : file_get_contents(__DIR__ . '/google-cloud-storage.json');

        /** @var GoogleStorageClient client */
        $this->client = new GoogleStorageClient([
            'keyFile' => json_decode($keyFile, true),
        ]);

        /** @var Bucket bucket */
        $this->bucket = $this->client->bucket(Arr::get($configs, 'bucketName', ''));
        $this->folder = Arr::get($configs, 'folder', '');
        $this->encryptionKey = Arr::get($configs, 'encryptionKey', '');
        $this->encryptionKeySHA256 = Arr::get($configs, 'encryptionKeySHA256', '');
        $this->expires = Carbon::now()->addMinutes(Arr::get($configs, 'expireMinutes', 15));
        $this->storageApiUri = Arr::get($configs, 'storageApiUri', '');
    }

}
